package kkurawal;

import kkurawal.utils.DirectPrinter;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Konglie on 7/25/2016.
 */
public class PrintDemo extends JFrame {
	public PrintDemo(){
		super("Demo Direct Printing");
		buildGUI();
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(640,360));
		setLocationRelativeTo(null);
	}

	private void buildGUI(){
		Border padding = BorderFactory.createEmptyBorder(5,5,5,5);

		JLabel header = new JLabel("Masukkan teks yang ingin dicetak");
		header.setBorder(padding);
		header.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(header, BorderLayout.NORTH);

		JPanel body = new JPanel(new BorderLayout());
		body.setBorder(padding);
		final JTextArea ta = new JTextArea();
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		bindPasteMenu(ta);
		body.add(new JScrollPane(ta), BorderLayout.CENTER);

		JPanel btns = new JPanel(new FlowLayout());
		JButton btnDirectPrint = new JButton("Cetak (Langsung)");
		btns.add(btnDirectPrint);
		btnDirectPrint.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new DirectPrinter().print(ta.getPrintable(null, null));
			}
		});

		JButton btnDialogPrint = new JButton("Cetak (pilih Printer)");
		btnDialogPrint.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new DirectPrinter().print(ta.getPrintable(null, null), true);
			}
		});
		btns.add(btnDialogPrint);
		body.add(btns, BorderLayout.SOUTH);

		getContentPane().add(body, BorderLayout.CENTER);

		JLabel footer = new JLabel("<html><center>Created by Konglie<br/>konglie@kurungkurawal.com</center></html>");
		footer.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		footer.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(footer, BorderLayout.SOUTH);
	}

	private void bindPasteMenu(final JTextArea ta){
		final JPopupMenu pop = new JPopupMenu();
		JMenuItem paste = new JMenuItem("Paste");
		pop.add(paste);

		paste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					ta.setText((String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});

		ta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e)){
					pop.show(ta, e.getX(), e.getY());
				}
			}
		});
	}


	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new PrintDemo()
						.setVisible(true);
			}
		});
	}
}
