package kkurawal.utils;

/**
 * Created by Konglie on 10/24/2015.
 */

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.PageRanges;
import javax.swing.*;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;


public class DirectPrinter {

    public DirectPrinter() {

    }

	public void print(Printable doc){
		this.print(doc, false);
	}

	public void print(Printable doc, boolean showDialog){
		PrintRequestAttributeSet asset = new HashPrintRequestAttributeSet();
		asset.add(new PageRanges(1, 1));
		asset.add(new Copies(1));

		PrinterJob printJob = PrinterJob.getPrinterJob();
		printJob.setJobName("Demo Printing by konglie@kurungkurawal.com");
		if(showDialog && !printJob.printDialog()){
			JOptionPane.showMessageDialog(null, "PRINT Cancelled");
			return;
		}

		printJob.setPrintable(doc);
		try {
			printJob.print(asset);
		} catch (Exception err) {
			System.err.println(err);
			JOptionPane.showMessageDialog(null, "PRINT ERROR: \n" + err.getMessage());
		}
	}
}
